package lt.vcs;

import java.util.Scanner;

public class main {
   
    public static void main(String[] args) {  
        int skaicius = 0;
        int suma = 0;
        
        while (!"0".equals(skaicius)){
            System.out.println("Iveskite skaiciu. Norint pabaigti programa - iveskite 0");
            skaicius = new Scanner(System.in).nextInt();
            suma+=skaicius;
            if (skaicius == 0){
                System.out.println("Jus ivedete 0");                
                System.out.println("Visu ivestu skaicius suma lygi: " + suma);
                System.exit(0);
            }            
        }         
    } 
//    public static int suma (int...skaiciai){
//        int result = 0;
//        for (int i=0; i<skaiciai.length; i++){
//            result += skaiciai[i];
//        }
//        return result;
//    }
}
