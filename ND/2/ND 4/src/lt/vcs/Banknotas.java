/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

/**
 *
 * @author Administrator
 */
public enum Banknotas 
{
    PENKI(5),
    DESIMT(10),
    DVIDESIMT(20),
    PENKIASDIASIMT(50),
    SIMTAS(100),
    DUSIMTAI(200),
    PENKISIMTAI(500);
    
    private int sk;

    public int getSk() {
        return sk;
    }

    public void setSk(int sk) {
        this.sk = sk;
    }
    
    private Banknotas(int sk)
    {
       this.sk = sk; 
    }
    
    
}
