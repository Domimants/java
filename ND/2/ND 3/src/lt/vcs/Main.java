/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import static lt.vcs.VcsUtils.*;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        //b)
//        
       String zodziai = inStr("ivesk zodius kuriuos skirsi kableliais");
       String[] strMas = zodziai.split(",");
       for(int i=0; i<=strMas.length-1; i++)
       {
           out(i+" "+strMas[i]);
       }
       
        //c)
        
        String zodis = inLine("iveskite zodi arba sakini");
        String[] strZod = zodis.split("");
        int x=0;
        int y=0;
        for(int i=0; i<=strZod.length-1; i++)
        {
            y++;
            if("a".equals(strZod[i]))
            {
                x++;
            }
        }
        out("is viso raidziu yra:"+y+", o raidziu a: "+x);
       
    }
    
}
