package lt.vcs;
import static lt.vcs.VcsUtils.*;
import static lt.vcs.Bankomatas.*;
import static lt.vcs.Person.*;

/**
 *
 * @author Gabriele
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Bankomatas bank = new Bankomatas();
        while(bank.getCash()>0){
            Person per = new Person(inStr("Iveskite savo varda"));
            
            String pin = inStr("Iveskite pin koda");
            boolean x = logIn(pin);
            if(x==false){
                out("neteisingai ivestas pin koodas");
                break;
            }
            out(per.getName()+", bankomate liko "+bank.getCash());
            int pinigai = inInt("Kiek pinigu noresite isimti?");
            if(pinigai>bank.getCash()){
                out("Tiek pinigu bankomate nera");
                break;
            }
            bank.isimti(pinigai);
        }
        
        
    }

    
}
