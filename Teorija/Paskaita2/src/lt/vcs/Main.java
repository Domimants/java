package lt.vcs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Cukrus
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String ivestas = null;
        while (true) {
            ivestas = inStr("Ivesk betkoki zodi");
            out(ivestas);
            if (ivestas.equals("pabaiga") || ivestas.equals("end") || ivestas.equals("exit")) {
                break;
            }
        }
        
        int result = 0;
        while (true) {
            int i = inInt("Ivesk sveika skaiciu");
            if (i == 0) {
                break;
            } else {
                result += i;
            }
        }
        out("rezultatas: " + result);
        
        switch (ivestas) {
            case "pabaiga":
                //kodas jei tekstas pabaiga
                //break;
            case "end":
                //kodas jei tekstas end
                break;
            case "exit":
                ////kodas jei tekstas exit
                break;
            default:
                //jei neatitiko neivienai salyugai pries tai
        }
        int i = inInt("ivesk sveika sk");
        switch (i) {
            case 1:
            case 2:
            case 3:
                out("kazkox tekstas");
                break;
            case 4:
            case 5:
            case 6:
                out("kazkox kitas tekstas");
                break;
            default:
                //jei neatitiko neivienai salyugai pries tai
        }
        int[] mas = i>0 ? new int[i] : new int[5];
        int[] mas2 = {1,2,3,4,5};
        String[] mas3 = {"viens", "du", "trys"};
        int[] mas4;
        Object o = "sdfdsf";
        List<String> mas5 = Arrays.asList(mas3);
        
        String formatinsim = "mano batai buvo %d";
        out(String.format(formatinsim, 7));
        out(formatinsim);
        
        String kint = "melagis, melagis";
        if (kint != null) {
            kint.isEmpty();
        }
        kint = kint.replaceAll(" ", "");
        out(""+kint.matches("mel.*"));
        String[] strMas = kint.split(",");
        for (String a : strMas) {
            out(a);
        }
        
        for (int a = mas.length-1; a>0 ; a--) {
            mas[a] = a;
        }
        for (int a : mas) {
            out("" + a);
        }
        out("rezultate i lygus: "+i);
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd '['HH:mm:ss']'");
        Date data = new Date();
        out(data);
        out(sdf.format(data));
        for (int ii=0; ii<5; ii++) {
            out("random"+ii+": " + random(1, 6));
        }
    }
    
}
